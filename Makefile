TESTFLAGS=-fprofile-arcs -ftest-coverage
clean:
	rm -f *.o testhasard testcomparaison testapp *.gcov *.gcda *.gcno

testcomparaison: testcomparaison.c comparaison.h comparaison.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -c testcomparaison.c comparaison.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testcomparaison testcomparaison.o comparaison.o
	./testcomparaison
	gcov -c -p testcomparaison

testhasard: testhasard.c hasard.h hasard.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -c testhasard.c hasard.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testhasard testhasard.o hasard.o
	./testhasard
	gcov -c -p testhasard
