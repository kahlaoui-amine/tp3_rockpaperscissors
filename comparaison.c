#include <stdio.h> 
#include <stdlib.h> 
#include <time.h>
#include "comparaison.h"

int comparaison (char choixJoueur, char choixOrdinateur) {
    if (choixJoueur == choixOrdinateur) {
        return 0; 
    }  else if ((choixJoueur == 'R' && choixOrdinateur == 'C') || 
                (choixJoueur == 'P' && choixOrdinateur == 'R') || 
                (choixJoueur == 'C' && choixOrdinateur == 'P')) { 
        return 1; 
} else {
return -1; 
}
}