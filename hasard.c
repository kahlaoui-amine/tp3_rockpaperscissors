#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "hasard.h"
// Fonction pour obtenir un choix aléatoire de l'ordinateur (R, P, ou C) 
char hasard() {
	srand(time(NULL));
	int nombreAleatoire = rand() % 3;
	if (nombreAleatoire == 0) {
		return 'R';
	} else if (nombreAleatoire == 1) {
	return 'P';
	} else {
	return 'C';
	}
}
